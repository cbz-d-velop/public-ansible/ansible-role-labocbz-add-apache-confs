# Ansible role: labocbz.add_apache_configuration

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: Apache2](https://img.shields.io/badge/Tech-Apache2-orange)
![Tag: HTTP](https://img.shields.io/badge/Tech-HTTP-orange)
![Tag: HTTPS](https://img.shields.io/badge/Tech-HTTPS-orange)

An Ansible role create and add Apache2 HTTP/HTTPS confs to your server.

This role is designed to create Apache2 configurations tailored for various web applications. It offers a wide range of configuration options, allowing administrators to customize Apache2 settings based on their specific needs.

The role supports LDAP authentication, enabling administrators to integrate Apache2 with an LDAP server for user authentication. By specifying the LDAP URL, port, and domain components, administrators can easily set up LDAP-based authentication for their web applications.

It also provides options to set up virtual hosts for different server names and aliases, making it convenient to host multiple web applications on the same Apache2 server.

Administrators can define black-hole hosts, which will not be served by Apache2, effectively blocking access to specific domains.

SSL support is available, allowing administrators to configure HTTPS for their web applications. They can specify the SSL key and certificate files to enable secure connections.

Proxy pass functionality is also supported, enabling administrators to redirect requests to a backend server or another URL using the Apache2 proxy module.

The role offers fine-grained control over the ModSecurity module, allowing administrators to enable or disable specific rules based on their security requirements.

Furthermore, administrators can define rewrite rules to modify URLs and redirect specific requests to different locations.

The role also allows for URL-based exceptions from LDAP authentication, useful for scenarios where certain routes should not require authentication.

For administrators looking to manage multiple Apache2 configurations easily, this role is a valuable addition to the basic Apache2 installation. Its flexibility and extensive feature set make it a powerful tool for creating custom Apache2 setups to cater to various web application requirements.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-05-17: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-05-30: Cryptographic update

* SSL/TLS Materials are not handled by the role
* Certs/CA have to be installed previously/after this role use

### 2023-08-15: Big update

* Role now can use the v2 version of install_apache from labocbz
* Role have now a module based approche instead of code repetition for included j2 based of vars
* Role can now have custom, in the ./templates/custom you can put your j2 custom conf and import these conf directly via the custom_confs.name property
* Role have a better render aspect in the final conf
* Some fix are present
* Header, Pagespeed, Cookies, etc are now global conf, in the install_apache role, but you can do your by create a custom conf !
* Better var naming
* If a conf use HTTPS, role redirect trafic to HTTPS directly instead of rendering 2 files completly (see code for more details)
* As an example, a phpmyadmin custom_conf is present, but not working because its required more vars, so it's just an example, please check if before use it

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-11-16: Redirection

* HTTPS redirection is now based on %{SERVER_PORT}

### 2023-11-28: Logs and Redirect

* Fix the redirection
* Add ENV for no log

### 2023-12-14: System users

* Role can now use system users and address groups

### 2023-27-12: Preserve host

* You can now disable the preverse host option (in cause on empty request / logs)

### 2024-01-20: http-check

* Added a conf for /http-check as LB can check the app
* Added a log exclusion for the check

### 2024-02-22: New CICD and fixes

* Added support for Ubuntu 22
* Added support for Debian 11/22
* Edited vars for linting (role name and __)
* Fix idempotency
* New CI, need work on tag and releases
* CI use now Sonarqube

### 2024-05-05: LDAP Basic Auth

* LDAP handling have been fixed

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-06-21: Clear configuration files and realm

* Clear all configurations with spaces and line skip
* Clear main configurations files
* Added the server name inside the authentication realm

### 2024-10-20: Global refactoring

* New bootstrap with the automation
* Git hooks added
* New var naming system
* Simplification and handle one configuration instead of multiples
* Added test for each type of configurations

### 2024-12-10: LDAP custom auth cookie

* LDAP Authentication is handled by a custom cookie
* Cookie is 32 char long and randomized
* Configuration are not idempotent, because of the random provided for the cookie
* /ldap-authentication file added to sign in
* No need for disabled routes

### 2025-01-01: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

### 2025-01-25: Nextcloud fixes

* Edit body sizes
* Edit reverse proxies configurations

### 2025-01-30: Open URIs for LDAP

* You can now remove LDAP restriction for specific URIs patterns

### 2025-02-11: LDAP redirections

* Fix LDAP redirection after sucessfull auth

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
